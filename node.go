package caplate

import (
	"html/template"
	"math/rand"
	"strings"
)

type nodeInternal struct {
	cachedTemplate string
	idInternal     string
	builtTemplate  *template.Template
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func (n *nodeInternal) buildId() {
	b := make([]byte, 12)
	for i := range b {
		b[i] = letterBytes[rand.Int63()%int64(len(letterBytes))]
	}
	n.idInternal = string(b)
}

func (n *nodeInternal) id() string {
	return n.idInternal
}

func (n *nodeInternal) isBuilt() bool {
	return n.builtTemplate != nil
}

func (n *nodeInternal) getBuiltTemplate() *template.Template {
	return n.builtTemplate
}

func (n *nodeInternal) setBuiltTemplate(t *template.Template) {
	n.builtTemplate = t
}

type Node interface {
	id() string
	props() map[string]interface{}
	children() []Node
	// unparsed template to execute Node as the root of a template
	template() string
	isBuilt() bool
	getBuiltTemplate() *template.Template
	setBuiltTemplate(*template.Template)
}

func templateAsReference(n Node) string {
	b := strings.Builder{}
	b.WriteString(`{{template "`)
	b.WriteString(n.id())
	b.WriteString(`" .`)
	b.WriteString(n.id())
	b.WriteString(`}}`)
	return b.String()
}

func templateAsDefinition(n Node) string {
	b := strings.Builder{}
	b.WriteString(`{{define "`)
	b.WriteString(n.id())
	b.WriteString(`"}}`)
	b.WriteString(n.template())
	b.WriteString(`{{end}}`)
	return b.String()
}
