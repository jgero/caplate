# Caplate

The package `caplate` contains wrappers for the `html/template` package for commonly used components
and use-cases.

When building webpages with HTML many parts of the templates can be reused for different routes.
Doing that with different template files or even with strings mixed into the codebase of the
business logic is suboptimal. This is where `caplate` comes into play. The goal is to outsource
template management and build UI components that can be added together as needed. By encapsulating
components in go structs they can be handled easily and building modular UIs should become trivial.

## Examples

The start is always to create some nodes and appending them together as desired. Currently the
selection of components and layouts is pretty limited but there is a lot more to come. As you can
see the button is added twice. Because `caplate` works with pointers any changes to the
`confirmButton` variable will be reflected everywhere this specific button pointer was appended.
This enables propagating changes across multiple templates by changing just a single property.

```go
d := caplate.NewDocument("en", "my page")
d.MetaTags = append(d.MetaTags, &caplate.MetaTag{
	Name:    metatagName,
	Content: metatagContent,
})
base := caplate.NewHorizontalLayout()
d.AppendChild(base)
confirmButton := caplate.NewButton("confirm")
base.AppendChild(confirmButton)
base.AppendChild(confirmButton)
```

When a tree is built from appending some nodes a template can be generated with the help of the
`BuildTemplate` function of the package. It is strongly recommended to build the template only once,
because this process is way costlier than executing the templates with different parameters. So when
using this package to render HTML pages for a webserver run the `BuildTemplate` at server startup.

```go
err := caplate.BuildTemplate(d)
```

While the tree of nodes cannot be changed after building the template, the properties of the nodes
are still modifiable. When calling `ExecuteTemplate` the provided template (which should be
generated from the same nodes, otherwise it will lead to errors) will be executed with the current
state of the properties.

```go
out, err := caplate.ExecuteTemplate(d)
```

## License

MIT licensed. See LICENSE file for details.
