package caplate

import "strings"

type Button struct {
	*nodeInternal
	Classes string
	Label   string
}

func NewButton(label string) *Button {
	b := &Button{
		Label:        label,
		nodeInternal: &nodeInternal{},
	}
	b.buildId()
	return b
}

func (b *Button) children() []Node {
	return nil
}

func (b *Button) template() string {
	if b.cachedTemplate != "" {
		return b.cachedTemplate
	}
	builder := strings.Builder{}
	builder.WriteString(`<button class="`)
	builder.WriteString(b.id())
	builder.WriteString(` {{.Classes}}">{{.Label}}</button>`)
	b.cachedTemplate = builder.String()
	return b.cachedTemplate
}

func (b *Button) props() map[string]interface{} {
	return map[string]interface{}{
		"Classes": b.Classes,
		"Label":   b.Label,
	}
}
