package caplate_test

import (
	"errors"
	"io"
	"strings"
	"testing"

	"gitlab.com/jgero/caplate"
	"golang.org/x/net/html"
)

func TestDocument(t *testing.T) {
	lang := "en"
	title := "my test doc"
	metatagName := "testname"
	metatagContent := "metatagcontent"
	d := caplate.NewDocument(lang, title)
	d.MetaTags = append(d.MetaTags, &caplate.MetaTag{
		Name:    metatagName,
		Content: metatagContent,
	})
	base := caplate.NewHorizontalLayout()
	d.AppendChild(base)
	confirmButton := caplate.NewButton("confirm")
	base.AppendChild(confirmButton)
	err := caplate.BuildTemplate(d)
	if err != nil {
		t.Fatalf("expected template to build without error: %s", err)
	}
	out, err := caplate.ExecuteTemplate(d)
	if err != nil {
		t.Fatalf("expected template to execute without error: %s", err)
	}
	t.Log(out)
	if out == "" {
		t.Errorf("expected template to not be empty")
	}

	wasMetatagVisisted := false
	tokenizer := html.NewTokenizer(strings.NewReader(out))
tokenizerLoop:
	for {
		tokenType := tokenizer.Next()
		token := tokenizer.Token()
		tokenName := token.Data
		switch tokenType {
		case html.ErrorToken:
			t.Log(tokenizer.Err())
			if errors.Is(tokenizer.Err(), io.EOF) {
				break tokenizerLoop
			}
			t.Errorf("no error token expected")
		case html.StartTagToken, html.SelfClosingTagToken:
			t.Logf("%s token has %d attrs", tokenName, len(token.Attr))
			if tokenName == "html" {
				for _, v := range token.Attr {
					if v.Key == "lang" {
						if v.Val == lang {
							continue tokenizerLoop
						} else {
							t.Errorf("expected lang attribute to be %s but is %s", lang, v.Val)
						}
					}
				}
				t.Errorf("expected html token to have lang")
			}
			if tokenName == "meta" {
				foundName := false
				foundContent := false
				for _, v := range token.Attr {
					if v.Key == "name" && v.Val == metatagName {
						t.Log("found metatag name")
						foundName = true
					}
					if v.Key == "content" && v.Val == metatagContent {
						t.Log("found metatag content")
						foundContent = true
					}
				}
				if foundName && foundContent {
					t.Log("confirmed metatag visit")
					wasMetatagVisisted = true
				}
			}
		}
	}
	if !wasMetatagVisisted {
		t.Errorf("expected to find meta tag with name '%s' and content '%s' but there was none", metatagName, metatagContent)
	}
}
