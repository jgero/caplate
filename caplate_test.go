package caplate_test

import (
	"errors"
	"io"
	"strings"
	"testing"

	"gitlab.com/jgero/caplate"
	"golang.org/x/net/html"
)

func TestPropChange(t *testing.T) {
	root := caplate.NewHorizontalLayout()
	button := caplate.NewButton("my button")
	root.AppendChild(button)
	root.AppendChild(button)
	err := caplate.BuildTemplate(root)
	if err != nil {
		t.Fatalf("expected template to build without error: %s", err)
	}
	res, err := caplate.ExecuteTemplate(root)
	if err != nil {
		t.Fatalf("expected template to execute without error: %s", err)
	}
	t.Log(res)

	expectedTokens := []string{"div", "button", "button", "button", "button", "div"}
	expectedText := []string{"my button", "my button"}
	htmlChecker(t, expectedTokens, expectedText, res)

	button.Label = "my new label"
	res, err = caplate.ExecuteTemplate(root)
	if err != nil {
		t.Fatalf("expected template to execute without error: %s", err)
	}

	expectedTokens = []string{"div", "button", "button", "button", "button", "div"}
	expectedText = []string{"my new label", "my new label"}
	htmlChecker(t, expectedTokens, expectedText, res)
}

func htmlChecker(t *testing.T, expectedTokens []string, expectedText []string, htmlString string) {
	i := 0
	j := 0
	tokenizer := html.NewTokenizer(strings.NewReader(htmlString))
tokenizerLoop:
	for {
		token := tokenizer.Next()
		tokenName := tokenizer.Token().Data
		switch token {
		case html.ErrorToken:
			if errors.Is(tokenizer.Err(), io.EOF) {
				break tokenizerLoop
			}
			t.Log(tokenizer.Err())
			t.Errorf("no error token expected")
		case html.StartTagToken:
			t.Log(tokenName)
			if tokenName != expectedTokens[i] {
				t.Errorf("unexpected token %s, expected %s", tokenName, expectedTokens[i])
			}
			i = i + 1
		case html.EndTagToken:
			t.Log(tokenName)
			if tokenName != expectedTokens[i] {
				t.Errorf("unexpected token %s, expected %s", tokenName, expectedTokens[i])
			}
			i = i + 1
		case html.TextToken:
			t.Log(tokenName)
			if tokenName != expectedText[j] {
				t.Errorf("unexpected text %s, expected %s", tokenName, expectedText[j])
			}
			j = j + 1
		}
	}
	if i != len(expectedTokens) {
		t.Errorf("unexpected token count, expected %d but got %d", len(expectedTokens), i)
	}
	if j != len(expectedText) {
		t.Errorf("unexpected text count, expected %d but got %d", len(expectedText), j)
	}
}
