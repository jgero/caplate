package caplate

import (
	"strings"
)

type Document struct {
	*nodeInternal
	bodyNodes []Node
	Language  string
	Title     string
	MetaTags  []*MetaTag
}

type MetaTag struct {
	Name    string
	Content string
}

func NewDocument(lang string, title string) *Document {
	d := &Document{
		nodeInternal: &nodeInternal{},
		MetaTags:     make([]*MetaTag, 0),
		Language:     lang,
		Title:        title,
	}
	d.buildId()
	return d
}

func (d *Document) AppendChild(newChild Node) {
	// clear cache of template
	d.cachedTemplate = ""
	d.bodyNodes = append(d.bodyNodes, newChild)
}

func (d *Document) children() []Node {
	return d.bodyNodes
}

func (d *Document) template() string {
	if d.cachedTemplate != "" {
		return d.cachedTemplate
	}
	b := strings.Builder{}
	b.WriteString(`<!DOCTYPE html><html lang="{{.Language}}">`)
	b.WriteString(`<head><title>{{.Title}}</title><meta charset="UTF-8">`)
	b.WriteString(`<meta name="viewport" content="width=device-width, initial-scale=1">`)
	b.WriteString(`{{range .MetaTags}}<meta name="{{.Name}}" content="{{.Content}}">{{end}}`)
	b.WriteString(`</head><body>`)
	for _, v := range d.bodyNodes {
		b.WriteString(templateAsReference(v))
	}
	b.WriteString(`</body></html>`)
	d.cachedTemplate = b.String()
	return d.cachedTemplate
}

func (d *Document) props() map[string]interface{} {
	props := map[string]interface{}{
		"Title":    d.Title,
		"MetaTags": d.MetaTags,
		"Language": d.Language,
	}
	for _, v := range d.bodyNodes {
		props[v.id()] = v.props()
	}
	return props
}
