package caplate

import (
	"bytes"
	"fmt"
	"html/template"
	"strings"
	"sync"
)

type GenerateTemplateError struct {
	err error
}

func (err *GenerateTemplateError) Error() string {
	return fmt.Sprintf("failed to generate template: %s", err.err)
}

func (err *GenerateTemplateError) Unwrap() error {
	return err.err
}

type ExecuteTemplateError struct {
	err error
}

func (err *ExecuteTemplateError) Error() string {
	return fmt.Sprintf("failed to execute template: %s", err.err)
}

func (err *ExecuteTemplateError) Unwrap() error {
	return err.err
}

var bufPool = sync.Pool{
	New: func() interface{} {
		return new(bytes.Buffer)
	},
}

// building is costly, call this once and reuse the result
func BuildTemplate(root Node) error {
	if root.isBuilt() {
		return &GenerateTemplateError{fmt.Errorf("template was already built")}
	}
	partialSet := make(map[string]struct{}, 0)
	partialSet[root.template()] = struct{}{}
	for _, c := range root.children() {
		traverseNodes(c, partialSet)
	}
	b := strings.Builder{}
	for partial := range partialSet {
		b.WriteString(partial)
	}
	tmpl, err := template.New("caplate").Parse(b.String())
	if err != nil {
		return &GenerateTemplateError{err}
	}
	root.setBuiltTemplate(tmpl)
	return nil
}

func traverseNodes(n Node, m map[string]struct{}) {
	m[templateAsDefinition(n)] = struct{}{}
	for _, v := range n.children() {
		traverseNodes(v, m)
	}
}

// executing is relatively cheap, build once and only execute on every request
func ExecuteTemplate(root Node) (string, error) {
	b := bufPool.Get().(*bytes.Buffer)
	b.Reset()
	defer bufPool.Put(b)

	if !root.isBuilt() {
		return "", &ExecuteTemplateError{fmt.Errorf("template is not built yet")}
	}

	err := root.getBuiltTemplate().Execute(b, root.props())
	if err != nil {
		return "", &ExecuteTemplateError{err}
	}
	return b.String(), nil
}
