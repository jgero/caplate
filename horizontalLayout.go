package caplate

import (
	"strings"
)

type HorizontalLayout struct {
	*nodeInternal
	layoutItems []Node
	Classes     string
}

func NewHorizontalLayout() *HorizontalLayout {
	n := &HorizontalLayout{
		nodeInternal: &nodeInternal{},
	}
	n.buildId()
	return n
}

func (n *HorizontalLayout) AppendChild(newChild Node) {
	n.cachedTemplate = ""
	n.layoutItems = append(n.layoutItems, newChild)
}

func (n *HorizontalLayout) children() []Node {
	return n.layoutItems
}

func (n *HorizontalLayout) template() string {
	if n.cachedTemplate != "" {
		return n.cachedTemplate
	}
	b := strings.Builder{}
	b.WriteString(`<div class="`)
	b.WriteString(n.id())
	b.WriteString(` {{.Classes}}">`)
	for _, v := range n.layoutItems {
		b.WriteString(templateAsReference(v))
	}
	b.WriteString(`</div>`)
	n.cachedTemplate = b.String()
	return n.cachedTemplate
}

func (n *HorizontalLayout) props() map[string]interface{} {
	props := map[string]interface{}{
		"Classes": n.Classes,
	}
	for _, v := range n.layoutItems {
		props[v.id()] = v.props()
	}
	return props
}
